// Map script

var mymap = L.map('mapid').setView([-1.286389, 36.817223], 10);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiYmttbWIiLCJhIjoiY2tiYnowNXR4MDVqcTJwcnF2eXR2NzM5aSJ9.ve8hubwcXV2qHNZAWmjx5A'}).addTo(mymap);

var circle = L.circle([-1.286389, 36.817223], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.25,
        radius: 5000
}).addTo(mymap);


var popup = L.popup();
var deliverylocation;
var currenttime = Date.UTC();
function onMapClick(e) {
    popup
         .setLatLng(e.latlng)
         .setContent("Delivery location")
         .openOn(mymap);
    deliverylocation = e.latlng.toString();
    document.cookie = "deliverylocation" + "=" + deliverylocation + ";path=/";
}
mymap.on('click', onMapClick);

